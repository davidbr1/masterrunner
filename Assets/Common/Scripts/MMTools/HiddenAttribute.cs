﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using UnityEditor;

namespace MoreMountains.Tools
{	
	public class HiddenAttribute : PropertyAttribute { }
}
#endif