﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace MoreMountains.InfiniteRunnerEngine
{
	public class SetSpawnPosition : MonoBehaviour {

		//Y-Pos of spawned object in world space
		[Header("Y-Position if spawn ist not random")]
		public float Yposition;

		//Random Spawning true?
		[Header("If Y-Position is random")]
		public float[] RandomYpos;

		[Header("Overwrite Scale of Spawner")]
		public float realScale;	

		[Header("Overwrite Z-Rotation of Spawner")]
		public float realRotation;	

		[Header("Debugging only")]
		public float realYpos;

		//Is it collectible?
		public bool isCollectible = false;
		public float checkRadius;

		private bool isConflicting;



		// Use this for initialization
		void Start () {
			
			

			if (RandomYpos.Length > 0) {
				int randomNumber = (int)Mathf.Round (Random.Range (0, RandomYpos.Length));
				transform.position = new Vector2 (transform.position.x, RandomYpos [randomNumber]);
				realYpos = RandomYpos [randomNumber];


			} else {
				transform.position = new Vector2 (transform.position.x, Yposition);				
				realYpos = Yposition;
			}
		}




		void Update () {

				if (transform.position.y != realYpos) 
					transform.position = new Vector2 (transform.position.x, realYpos);	
			
		}


		
	}
}
