﻿using UnityEngine;
using System.Collections;

namespace MoreMountains.InfiniteRunnerEngine
{	
	public class CannonballSpawner : MonoBehaviour {
		//All Cannonballs to spawn
		public GameObject cannonball;

		//Exclamation Mark
		public GameObject exMark;
		public GameObject canvas;
		//Spawn positions
		public float[] spawnPositions;
		public float minSpawnTime;
		public float maxSpawnTime;
		public float initialDelay;

		//Debugging Only
		public int lastSpawnPositionNumber = 99;
		//Choose Random Y-Position Item and set it as last spawn position
		public int spawnPositionNumber;


		// Use this for initialization
		void Start () {
			if (initialDelay > 0) {
				//If an initial delay is set, spawning will start at that time
				Invoke ("SpawnBall", initialDelay);
			} else {
				//If no initial delay is set, spawning will start at the minSpawnTime
				Invoke ("SpawnBall", minSpawnTime);
			}		
		}


		void SpawnBall() {	
			//Only Spawn while game in progress (not on pause, not during game over screen)
			if (GameManager.Instance.Status == GameManager.GameStatus.GameInProgress) {
			
				//Choose Random time in between range for the next spawn


				//choose random spawn position
				spawnPositionNumber = Random.Range (0, spawnPositions.Length);



				//Show Excl Mark and Cannonball
				StartCoroutine (ShowExMark (spawnPositions [spawnPositionNumber]));
				//Spawn Cannonball
				//Instantiate (cannonball, new Vector2(transform.position.x, spawnPositions[spawnPositionNumber] ), Quaternion.identity);

				lastSpawnPositionNumber = spawnPositionNumber;
				//Invoke next Spawn

				float randomTime = Random.Range (minSpawnTime, maxSpawnTime);
				Invoke ("SpawnBall", randomTime);
			} 
		}

		//Function to show Exclamation Mark Warning before cannonball
		IEnumerator ShowExMark(float yPos){			
			GameObject exMarkUi = Instantiate(exMark, transform.position, Quaternion.identity) as GameObject;

			exMarkUi.transform.SetParent (canvas.transform, false);
			RectTransform rectTransform = exMarkUi.GetComponent<RectTransform> ();

			if (yPos == -13) {
				rectTransform.anchoredPosition = new Vector2 (125, -47);			
			} else if (yPos == -12) {
				rectTransform.anchoredPosition = new Vector2 (125, -32);					
			} else if (yPos == -11) {
				rectTransform.anchoredPosition = new Vector2 (125, -17);	
			} else if (yPos == -10) {
				rectTransform.anchoredPosition = new Vector2 (125, -2);	
			} else if (yPos == -9) {
				rectTransform.anchoredPosition = new Vector2 (125, 13);	
			} else if (yPos == -8) {
				rectTransform.anchoredPosition = new Vector2 (125, 28);	
			} else if (yPos == -7) {
				rectTransform.anchoredPosition = new Vector2 (125, 43);	
			} else if (yPos == -6) {
				rectTransform.anchoredPosition = new Vector2 (125, 58);	
			}
			GetComponent<AudioSource> ().Play ();
			yield return new WaitForSeconds (0.5f);
			exMarkUi.SetActive (false);
			yield return new WaitForSeconds (0.5f);
			GetComponent<AudioSource> ().Play ();
			exMarkUi.SetActive (true);
			yield return new WaitForSeconds (0.5f);
			exMarkUi.SetActive (false);
			yield return new WaitForSeconds (0.5f);
			GetComponent<AudioSource> ().Play ();
			exMarkUi.SetActive (true);
			yield return new WaitForSeconds (0.5f);
			Destroy (exMarkUi);
			//yield return new WaitForSeconds (0.5f);
			Instantiate (cannonball, new Vector2(transform.position.x, spawnPositions[spawnPositionNumber] ), Quaternion.identity);
		}
	}
}