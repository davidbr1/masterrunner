﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace MoreMountains.InfiniteRunnerEngine
{
	public class ExMark : MonoBehaviour {

		public GameObject CannonBall;

		// Use this for initialization
		void Start () {			
			gameObject.GetComponent<Text>().enabled = false;	

		
		}

		// Update is called once per frame
		void Update () {
			//Find Clones of that particular Cannonball
			GameObject ballClone = GameObject.FindGameObjectWithTag(CannonBall.tag);
			if (ballClone != null) {
				if (ballClone.activeInHierarchy) {
					StartCoroutine ("activateExMark");
				} else {
					return;
				}
			}
			

		}

		IEnumerator activateExMark () {
			gameObject.GetComponent<Text>().enabled = true;	
			yield return new WaitForSeconds (2);
			gameObject.GetComponent<Text>().enabled = false;	
		}
	}
}
