﻿using UnityEngine;
using System.Collections;

namespace MoreMountains.InfiniteRunnerEngine {
	public class CharacterShoot : MonoBehaviour {

		public GameObject Bullet;
		public bool shootingBonus = false;
		public float waitBetweenShoot;
		public AudioClip emptyGun;
		public AudioClip shootingSound;
		public float fireRate;
		public float lastShot = 0;
		private RectTransform gunBarTransform;



		public float gunHealth = 1f;

		void Awake () {
			//Get the Gun Health Bar on Awake
			gunBarTransform = GameObject.Find ("BarForeground").GetComponent<RectTransform> ();
		}
		// Use this for initialization
		public void ShootOnHit () {			
			transform.parent.GetComponent<Animator> ().SetBool ("shooting", true);	
			if (Time.time - lastShot > fireRate) {
				if (gunHealth > 0) {
					StartCoroutine ("stopShooting");
					//Adjust GunHealth Bar after each shot
					gunHealth -= 0.1f;
					lastShot = Time.time;
				} else {
					SoundManager.Instance.PlaySound (emptyGun, transform.position);	
					lastShot = Time.time;
					transform.parent.GetComponent<Animator> ().SetBool ("shooting", false);
				}
			}
		}

		/*
		void OnTriggerEnter2D(Collider2D other) {
			if (other.tag == "Enemy" || other.tag == "Cannonball" ) {
				ShootOnHit ();
			}			
		}

		void OnTriggerStay2D(Collider2D stay) {
			if (stay.tag == "BuzzSaw") {
				ShootOnHit ();			
			}
		}*/

		//Constantly check, if gunHealth variable changed ( e.g. by shooting or by picking up the ammo power up) and adjust gun health bars size
		void Update () {
			gunBarTransform.localScale = new Vector3 (gunHealth, 1, 1);						
		}

		IEnumerator stopShooting() {
			Instantiate (Bullet, transform.position, Quaternion.identity);
			SoundManager.Instance.PlaySound (shootingSound, transform.position);
			yield return new WaitForSeconds (1f);
			transform.parent.GetComponent<Animator> ().SetBool ("shooting", false);
		}
	}
}
