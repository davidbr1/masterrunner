﻿using UnityEngine;
using System.Collections;

namespace MoreMountains.InfiniteRunnerEngine {
	public class MoveBetweenPoints : MonoBehaviour {
		public float yValueStart;
		public float yValueEnd;

		IEnumerator Start()
		{
			//Transform moves up and down in relation to parent
			while (true) {
				yield return StartCoroutine(MoveObject(transform, new Vector2 (transform.parent.position.x, yValueStart), new Vector2(transform.parent.position.x, yValueEnd), 3.0f));
				yield return StartCoroutine(MoveObject(transform, new Vector2(transform.parent.position.x, yValueEnd), new Vector2 (transform.parent.position.x, yValueStart), 3.0f));
			}
		}

		IEnumerator MoveObject(Transform thisTransform, Vector2 startPos, Vector2 endPos, float time)
		{
			var i= 0.0f;
			var rate= 1.0f/time;
			while (i < 1.0f) {
				i += Time.deltaTime * rate;
				thisTransform.position = Vector2.Lerp(startPos, endPos, i);
				yield return null; 
			}
		}
	}
}
