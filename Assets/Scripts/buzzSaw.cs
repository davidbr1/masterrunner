﻿using UnityEngine;
using System.Collections;
namespace MoreMountains.InfiniteRunnerEngine {
	public class buzzSaw : MonoBehaviour {
		public int lifes = 3;
		public bool onlyDeactivate;
		public string TagNameToHit;
		public AudioClip explosionSound;
		public GameObject ObjectExplosion;
		public float hurtFlashTime = 0.1f;
		public AudioClip hurtSound;
		public int pointsToAdd;
		


		// Use this for initialization


		void OnCollisionEnter2D(Collision2D coll) {

			if (coll.gameObject.tag == "Ground") {
				
				GetComponent<AudioSource> ().Play ();
			} else if (coll.gameObject.tag == "Player") {
				
				StartCoroutine (killIt ());
			}
			
		}

		IEnumerator killIt () {
			GameManager.Instance.AddPoints (pointsToAdd);
			//One life less per hit
			lifes -= 1;
			//Activate Hurt-Effect if more than 0 lifes left
			if (lifes > 0) {
				SoundManager.Instance.PlaySound (hurtSound, transform.position);
			}


			else  {
				if (onlyDeactivate) {
					gameObject.SetActive (false);
				} else {
					Destroy (gameObject);
				}

				if (ObjectExplosion != null) {
					Instantiate (ObjectExplosion, transform.position, Quaternion.identity);
				}
				if (explosionSound != null) {				
					SoundManager.Instance.PlaySound (explosionSound, transform.position);
				}

				yield return new WaitForSeconds (1);
			}

		} 


	}
}
